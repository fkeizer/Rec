#ifndef TRACKTOOLS_STATEDETAILEDBETHEBLOCHENERGYCORRECTIONTOOL_H
#define TRACKTOOLS_STATEDETAILEDBETHEBLOCHENERGYCORRECTIONTOOL_H 1

// Include files
// -------------

#include <unordered_map>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/PhysicalConstants.h"

// from TrackInterfaces
#include "TrackInterfaces/IStateCorrectionTool.h"

// from TrackEvent
#include "Event/State.h"

/** @class StateDetailedBetheBlochEnergyCorrectionTool
 *
 *  This state correction tool applies a dE/dx energy loss correction
 *  with the full version of the Bethe-Bloch equation.
 *
 *  @author Stephanie Hansmann-Menzemer
 *  @date   2008-05-02
 *
 */
class StateDetailedBetheBlochEnergyCorrectionTool : public extends<GaudiTool, IStateCorrectionTool> {
public:
  /// Standard constructor
  using base_class::base_class;

  StatusCode initialize() override;

  /// Correct a State for dE/dx energy losses with a simplified Bethe-Bloch equiaton
  void correctState( LHCb::State& state, const Material* material, ranges::v3::any& cache, double wallThickness,
                     bool upstream, double mass ) const override;

  ranges::v3::any createBuffer() const override { return MaterialCache(); }


private:
  // Job options
  Gaudi::Property<double> m_energyLossCorr { this,  "EnergyLossFactor",  1.0 };     ///< tunable energy loss correction
  Gaudi::Property<double> m_maxEnergyLoss { this,  "MaximumEnergyLoss", 100. * Gaudi::Units::MeV };      ///< maximum energy loss in dE/dx correction

  enum class Mat { X0, C, X1, a, m, DensityFactor, LogI };
  typedef std::unordered_map<const Material*, std::tuple<
      double, double, double, double, double, double, double> >
      Material2FactorMap;

	struct MaterialCache{
		Material2FactorMap mat2factors;
		Material2FactorMap::iterator lastCachedMaterial = std::end(mat2factors);
	};

};
#endif // TRACKTOOLS_STATEDETAILEDBETHEBLOChENERGYCORRECTIONTOOL_H
