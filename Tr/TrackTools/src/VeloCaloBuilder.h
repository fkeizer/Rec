#ifndef VELOCALOBUILDER_H
#define VELOCALOBUILDER_H 1

#include "GaudiAlg/GaudiTupleAlg.h"

#include "Event/Track.h"

#include "CaloInterfaces/ICaloGetterTool.h"

#include "Kernel/ILHCbMagnetSvc.h"

/** @class VeloCaloBuilder VeloCaloBuilder.h
 *
 * \brief  Make a ValoCaloTrack: Get calo clusters and match them to velo tracks. no trackfit done.
 *
 * Parameters:
 * - zcut: z position of the first velo state. for Ks reco reduces background
 * - IPcut: minimum pseudo IP.
 * - quali: minimum quality of the match (recommend 0.5 .. 1)
 * - VeloLoc, ClusterLoc, OutputTracks: TES locations of the input containers and the outputcontainer
 * - PtkickConstant: parameter for magnetic field
 * - zKick: z position of the magnet's bending plane (in cm !!!!)
 * - eRes0, eRes1: resolution of the calorimeter (energetic resolution)
 * - eCorrect: scal measured energy
 *
 *  @author Paul Seyfert
 *  @date   2010-09-16
 */

class VeloCaloBuilder : public GaudiTupleAlg {

 public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;
  /// initialization
  StatusCode initialize() override;
  /// execution
  StatusCode execute() override;

 private:
  // -- Methods
  float matchchi(LHCb::Track* velo, LHCb::Track* Calo); ///< calculate matching chi^2

  /// add track to output
  /// only 3 lines but there is more to be done in an elaborate study
  void TESpush(LHCb::Tracks* container, LHCb::Track* track,  LHCb::Track* ancestor, LHCb::Track* calotrack) const;

  // -- tools
  ILHCbMagnetSvc* m_magFieldSvc = nullptr;
  ICaloGetterTool* m_getter = nullptr;

  // -- properties
  Gaudi::Property<std::string> m_veloTracksName { this, "VeloLoc", LHCb::TrackLocation::Velo };
  Gaudi::Property<std::string> m_outputTracksName { this, "OutputTracks", "Rec/Track/VeloCalo" };
  Gaudi::Property<std::string> m_clusterlocation { this, "ClusterLoc", LHCb::CaloClusterLocation::Ecal };

  Gaudi::Property<float> m_ipcut { this,  "IPcut", 0 }; // min pseudo IP cut
  Gaudi::Property<float> m_zcut { this,  "zcut", 100 }; // whare the first Velo State should be
  Gaudi::Property<float> m_qualimodi { this,  "quali", 10 }; // min matching chi^2

  // -- variables from HltVeloEcalMatch
  // -- those now taken from N.Zwahlen's code. m_zKick also used for momentum
  Gaudi::Property<float> m_ptkickConstant { this, "PtkickConstant", 1.263f*(float)Gaudi::Units::GeV };
  Gaudi::Property<float> m_zKick { this, "zKick", 525.0 };
  Gaudi::Property<float> m_eres[2] = { { this, "eRes0", 0.60f }, { this, "eRes1", 0.70f } };
  Gaudi::Property<float> m_eCorrect { this, "eCorrect",  1.2f };

};
#endif
