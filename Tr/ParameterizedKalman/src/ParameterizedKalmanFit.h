#ifndef PARAMETERIZEDKALMANFIT_H 
#define PARAMETERIZEDKALMANFIT_H 1

// Include files
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "GaudiKernel/ToolHandle.h" 
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"

#include "Kernel/ILHCbMagnetSvc.h"

#include "Event/Track.h"

#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "KalmanParametrizations.h"
#include "ParameterizedKalmanFit_Methods.h"

#include <TTree.h>

using namespace ParKalman;

/** @class ParameterizedKalmanFit ParameterizedKalmanFit.h
 *
 *  Algorithm to perfrom a simplified Kalman filter based on parameterized extrapolations
 *
 *  Parameters:
 *  - UseUTHits:             Use hits in the UT in the fit
 *  - UseTHits:              Use hits in the SciFi in the fit
 *  - MaxNumOutlier:         Maximal number of outliers that should be removed
 *
 *  @author Simon Stemmle
 *  @date   2017-10-26
 */

class ParameterizedKalmanFit : public Gaudi::Functional::Transformer<
                                       LHCb::Tracks(const LHCb::Tracks&)> { 
public: 
  /// Standard constructor
  ParameterizedKalmanFit( const std::string& name, ISvcLocator* pSvcLocator );
  
  /// Algorithm initialization
  StatusCode initialize() override;   

  /// Algorithm execution
  LHCb::Tracks operator()(const LHCb::Tracks&) const override;

protected:

private:
  Gaudi::Property<bool> m_UseUT               {this, "UseUTHits"          , true};

  Gaudi::Property<bool> m_UseT                {this, "UseTHits"           , true};

  Gaudi::Property<int>  m_MaxNoutlier         {this, "MaxNumOutlier"      , 1   };
  
  Gaudi::Property<bool> m_UseBackwardEstiamte {this, "UseBackwMomEstiamte", true};
    
  //cache information for the smoother step
  bool m_do_smoother = true;

  //Parametrizations objects for each polarity
  KalmanParametrizations m_ParExtrUp   = KalmanParametrizations(Polarity::Up  );
  KalmanParametrizations m_ParExtrDown = KalmanParametrizations(Polarity::Down);
  
  //#####
  //Tools
  //#####
  //measuremant provider for the different detectors
  ToolHandle<IMeasurementProvider>  m_measProviderV  = 
                 {"MeasurementProviderT<MeasurementProviderTypes::VP>/VPMeasurementProvider", this};
  ToolHandle<IMeasurementProvider>  m_measProviderUT = 
                 {"MeasurementProviderT<MeasurementProviderTypes::UT>/UTMeasurementProvider", this}; 
  ToolHandle<IMeasurementProvider>  m_measProviderT  = 
                 {"FTMeasurementProvider/mMeasProvT", this};

  //extrapolators for the extrapolation to the beam pipe
  ToolHandle<ITrackExtrapolator> m_extrapolator_toPV = {"TrackMasterExtrapolator/extr", this};

  //Magnet field service in order to load the correct parameters
  ILHCbMagnetSvc* m_magFieldSvc = nullptr;

  //#################
  //1. Main method
  //#################

  /// Runs the Kalman filter on a track
  StatusCode fit(trackInfo &tI) const;

  //##########################################################################################
  // Methods for the Kalman filter that are not implemented in ParameterizedKalmanFit_Methods
  //##########################################################################################
  
  /// General method for predicting to a hit
  bool PredictState(int forward, int nHit, Gaudi::Vector5 &x, Gaudi::SymMatrix5x5 &C, double &lastz,
                    trackInfo &tI) const;
  
};

#endif //PARAMETERIZEDKALMANFIT_H
