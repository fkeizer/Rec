#ifndef PARAMETERIZEDKALMANFIT_CHECKER_H 
#define PARAMETERIZEDKALMANFIT_CHECKER_H 1

// Include files
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"

#include "Kernel/ILHCbMagnetSvc.h"

#include "Event/Track.h"

#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "MCInterfaces/IIdealStateCreator.h"

#include "KalmanParametrizations.h"
#include "ParameterizedKalmanFit_Methods.h"

#include "Associators/Associators.h"

#include <TTree.h>
#include <TFile.h>

using namespace ParKalman;

/** @class ParameterizedKalmanFit_Checker ParameterizedKalmanFit_Checker.h
 *
 *  Algorithm to perfrom a simplified Kalman filter based on parameterized extrapolations
 *  Version that creates tuples to track performance or to do the parameter tuning afterwards 
 *
 *  Parameters:
 *  - UseUTHits:             Use hits in the UT in the fit
 *  - UseTHits:              Use hits in the SciFi in the fit
 *  - MaxNumOutlier:         Maximal number of outliers that should be removed
 *  - 
 *  - RunForTuning:          Do Kalman steps using truth information to extract information for
 *                           tuning
 *  - OutputTreesFile:       Output location and file name for the tuning tuples
 *
 *  @author Simon Stemmle
 *  @date   2017-11-02
 */

struct trackTupleInfo;

class ParameterizedKalmanFit_Checker : public Gaudi::Functional::Transformer<
                                               LHCb::Tracks(const LHCb::Tracks&, const LHCb::ODIN&,
                                               const LHCb::LinksByKey& )> { 
public: 
  /// Standard constructor
  ParameterizedKalmanFit_Checker( const std::string& name, ISvcLocator* pSvcLocator );
  
  /// Algorithm initialization
  StatusCode initialize() override;   

  /// Algorithm execution
  LHCb::Tracks operator()(const LHCb::Tracks&, const LHCb::ODIN& odin,
                          const LHCb::LinksByKey& links) const override;

protected:

private:
  Gaudi::Property<bool>        m_UseUT                    {this, "UseUTHits",             
                                                           true                         };

  Gaudi::Property<bool>        m_UseT                     {this, "UseTHits",             
                                                           true                         };

  Gaudi::Property<int>         m_MaxNoutlier              {this, "MaxNumOutlier",
                                                           1                            };
  
  Gaudi::Property<bool>        m_UseBackwardEstiamte      {this, "UseBackwMomEstiamte",
                                                           true                         };
                                             
  Gaudi::Property<bool>        m_RunForTuning             {this, "RunForTuning",         
                                                           false                        };

  Gaudi::Property<std::string> m_TreesFileName            {this, "OutputTreesFile",      
                                                           "ParameterizedKalmanTuning"  };
  
  //Control options for tuning,testing
  bool m_SetTrueStateAfterUpdate     = false;
  bool m_SetTrueStateAfterPredict    = false;
  bool m_SetTrueStateAfterCreateSeed = false;

  //cache information for the smoother step
  bool m_do_smoother = true;

  //Parametrizations objects for each polarity
  KalmanParametrizations m_ParExtrUp   = KalmanParametrizations(Polarity::Up  );
  KalmanParametrizations m_ParExtrDown = KalmanParametrizations(Polarity::Down);
  
  //#####
  //Tools
  //#####
  ToolHandle<IMeasurementProvider>  m_measProviderV  =
                 {"MeasurementProviderT<MeasurementProviderTypes::VP>/VPMeasurementProvider", this};
  ToolHandle<IMeasurementProvider>  m_measProviderUT = 
                 {"MeasurementProviderT<MeasurementProviderTypes::UT>/UTMeasurementProvider", this}; 
  ToolHandle<IMeasurementProvider>  m_measProviderT  = 
                 {"FTMeasurementProvider", this};


  //ideal state creator for tuning and performance checks
  ToolHandle<IIdealStateCreator> m_idealStateCreator = {"IdealStateCreator", this};

  //extrapolators
  //1.For tuning and performance checks
  ToolHandle<ITrackExtrapolator> m_extrapolator      = {"TrackMasterExtrapolator/extr1", this};
  //2.For the extrapolation to the beam pipe
  ToolHandle<ITrackExtrapolator> m_extrapolator_toPV = {"TrackMasterExtrapolator/extr2", this};

  //Magnet field service in order to load the correct parameters
  ILHCbMagnetSvc* m_magFieldSvc = nullptr;

  

  //#################
  //1. Level methods
  //#################

  /// Method to run the Kalaman filter in order to extract tuning information
  StatusCode fitForTuning(trackInfo &tI, std::vector<TTree*> *trees, trackTupleInfo *tV) const;

  /// Runs the Kalman filter on a track
  StatusCode fit(trackInfo &tI, std::vector<TTree*> *trees, trackTupleInfo *tV) const;

  /// Create trees that should be filled for tuning and perfomance checks
  void addTrees(std::vector<TTree*> &trees, trackTupleInfo *treeVars) const;
  
  //####################################
  // Main methods for the Kalman filter
  //####################################

  /// Load hit information
  void LoadHits_Ch(trackInfo &tI, const ToolHandle<IMeasurementProvider> &m_measProviderV,
                   const ToolHandle<IMeasurementProvider> &m_measProviderUT,
                   const ToolHandle<IMeasurementProvider> &m_measProviderT, bool m_UseUT,
                   bool m_UseT, trackTupleInfo *tV) const;

  /// Method to create a seed state at the first Velo hit
  void CreateVeloSeedState_Ch(int nHit, Gaudi::Vector5 &x, Gaudi::SymMatrix5x5 &C, double &lastz,
                              trackInfo &tI, std::vector<TTree*> *trees, trackTupleInfo *tV) const;
  
  /// General method for updating at a hit
  void UpdateState_Ch(int forward, int nHit, Gaudi::Vector5 &x, Gaudi::SymMatrix5x5 &C,
                      double &lastz, trackInfo &tI, std::vector<TTree*> *trees,
                      trackTupleInfo *tV) const;
  
  /// General method for predicting to a hit
  bool PredictState_Ch(int forward, int nHit, Gaudi::Vector5 &x, Gaudi::SymMatrix5x5 &C,
                       double &lastz, trackInfo &tI, std::vector<TTree*> *trees,
                       trackTupleInfo *tV) const;
  
  /// Fill information for comparing default and this kalman filter
  void FillNtuple(Gaudi::Vector5 x, Gaudi::SymMatrix5x5 C, double z, trackInfo &tI,
                         trackTupleInfo *tV, double position, int pos) const;
 
  //#######################################
  // Further methods for the Kalman filter
  //#######################################
  
  /// Check if a MC particle is linked to this track
  int MatchesMC(const trackInfo &tI) const;
  
  /// Get true state at a given z position
  bool TrueState(double zpos, double& trueX, double& trueY, double& truetX, double& truetY,
                 double& trueqop, const trackInfo &tI, bool initialQop = true) const;
  
  /// Method to set the information of the default extrapolator for the tuning
  void fillInfoForExtrapolation(double z_prev, double z, trackInfo &tI, trackTupleInfo *tV) const;
  
  /// Predict UT <-> T precise version
  void PredictStateUTT_Ch(Gaudi::Vector5 &x, Gaudi::SymMatrix5x5 &C, double &lastz, trackInfo &tI,
                          std::vector<TTree*> *trees, trackTupleInfo *tV) const;
  
  /// Predict T(fixed z=7783) <-> first T layer
  void PredictStateTFT_Ch(int forward, Gaudi::Vector5 &x, Gaudi::SymMatrix5x5 &P, double &lastz,
                          trackInfo &tI, std::vector<TTree*> *trees, trackTupleInfo *tV) const;
  
};

//struct that contains variables for the tupling
struct trackTupleInfo {
  std::array<double, 5> m_x;
  std::array<double, 5> m_xTmp;
  std::array<double, 5> m_x_prev;
  std::array<double, 5> m_x_extr;
  std::array<double, 15> m_P;
  std::array<double, 15> m_P_extr;
  double m_z;
  double m_zTmp;
  double m_z_prev;

  std::array<double, 5> m_true_x;
  std::array<double, 5> m_true_xTmp;
  std::array<double, 5> m_true_x_prev;
  double m_true_z;
  double m_true_qop_here;
  int    m_MC_status;

  std::array<std::array<double, 5>, 3> m_sF_x;
  std::array<std::array<double, 15>, 3> m_sF_P;
  std::array<std::array<double, 5>, 3> m_sF_true_x;
  std::array<double, 3> m_sF_z;
  double m_sF_chi2;        
  double m_sF_chi2_V;        
  double m_sF_chi2_T;        
  double m_sF_chi2_UT;        
  double m_sF_ndof;        
  std::array<std::array<double, 5>, 3> m_dF_x;
  std::array<std::array<double, 15>, 3> m_dF_P;
  std::array<std::array<double, 5>, 3> m_dF_true_x;
  std::array<double, 3> m_dF_z;
  double m_dF_chi2;      
  double m_dF_ndof;      

  double m_hit_dxdy;
  double m_hit_dzdy;
  double m_hit_x0;
  double m_hit_y0;
  double m_hit_z0;
  double m_hit_x0_err;
  double m_hit_y0_err;
  
  //total number of hits (no matter if active or not)
  int m_NHitsTotal;
  //number of VELO hits
  int m_NHitsV;
  //number of UT station hits
  int m_NHitsUT;
  //number of T station hits
  int m_NHitsT;
  int m_NHit; 
};

#endif //PARAMETERIZEDKALMANFIT_CHECKER_H
