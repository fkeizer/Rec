#ifndef CHARGEDPARTICLESTOTRACKS_H
#define CHARGEDPARTICLESTOTRACKS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

#include "Event/Track.h"
#include "Event/Particle.h"

#include <string>
#include <vector>
#include <map>

class ITrackFitter;

/** @class ChargedParticlesToTracks ChargedParticlesToTracks.h
 *
 *
 *  @author Frederic Dupertuis
 *  @date   2012-10-08
 */
class ChargedParticlesToTracks : public GaudiHistoAlg {
public:
  /// Standard constructor
  ChargedParticlesToTracks( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:
  void DumpTracks(LHCb::Particle::ConstVector particles, LHCb::Track::Container& out);

  bool m_refit;

  float m_masswindow;
  float m_massoffset;

  ITrackFitter* m_trackFit = nullptr;
  ITrackFitter* m_trackPreFit = nullptr;

  std::vector<std::string> m_partloc;
  std::string m_trackOutputLocation;

  std::map<std::string,std::string> m_linesname;

};
#endif // CHARGEDPARTICLESTOTRACKS_H
