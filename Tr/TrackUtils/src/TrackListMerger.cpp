/** @class TrackListMerger TrackListMerger.h
 *
 *  Merge different track lists.
 *
 *  @author Wouter Hulsbergen
 *  @date   05/01/2010
 */

#include "GaudiAlg/ListTransformer.h"
#include <string>
#include "Event/Track.h"

using SelectionLists = Gaudi::Functional::vector_of_const_<LHCb::Track::Selection>;

struct TrackListMerger final
     : Gaudi::Functional::MergingTransformer<LHCb::Track::Selection(const SelectionLists&)>
{
  TrackListMerger(const std::string& name, ISvcLocator* pSvcLocator)
  : MergingTransformer( name, pSvcLocator,
                   { "inputLocations", {} },
                   { "outputLocation", {} } )
  { }

  LHCb::Track::Selection operator()(const SelectionLists& lists) const override
  {
    LHCb::Track::Selection out;
    for (const auto& list : lists) {
      for (const auto& track : list ) {
        // make sure the track is not yet there!
        if( std::find( out.begin(),out.end(),track) == out.end() )
          out.insert( track );
      }
    }
    return out;
  }
};

DECLARE_COMPONENT( TrackListMerger )
