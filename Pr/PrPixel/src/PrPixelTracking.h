#ifndef PRPIXELTRACKING_H
#define PRPIXELTRACKING_H 1

 //#define DEBUG_HISTO // fill some histograms while the algorithm runs.

// Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiAlg/ISequencerTimerTool.h"

// LHCb
#include "Event/RawEvent.h"

// Local
#include "PrKernel/IPrDebugTool.h"
#include "PrPixelHitManager.h"
#include "PrPixelTrack.h"

/** @class PrPixelTracking PrPixelTracking.h
 *  This is the main tracking for the Velo Pixel upgrade
 *
 *  @author Olivier Callot
 *  @author Sebastien Ponce
 */
class PrPixelTracking :
public Gaudi::Functional::MultiTransformer< std::tuple<LHCb::Tracks, LHCb::VPClusters>(const LHCb::RawEvent&)
#ifdef DEBUG_HISTO
                                           ,Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>
#endif
>{

 public:
  /// Standard constructor
  PrPixelTracking(const std::string &name, ISvcLocator *pSvcLocator);

  /// Algorithm initialization
  StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<LHCb::Tracks, LHCb::VPClusters> operator()(const LHCb::RawEvent&) const override;

 private:

  /// Extrapolate a seed track and try to add further hits.
  void extendTrack(const std::vector<PrPixelModuleHits>& modulehits,
                   PrPixelTrack& track,
                   const PrPixelHit& h1,
                   const PrPixelHit& h2,
                   bool debug) const;

  /// Search for tracks starting from pair of hits on adjacent sensors
  void searchByPair(const std::vector<PrPixelModuleHits>& modulehits,
                    PrPixelTracks& tracks) const;

  /// Produce LHCb::Track list understandable to other LHCb applications.
  void makeLHCbTracks(const PrPixelTracks& tracks,
                      LHCb::Tracks& outputTracks) const;

  /// Try to add a matching hit on a given module.
  const PrPixelHit *bestHit(const PrPixelModuleHits& module,
                            float module_z,
                            const float xTol,
                            const float maxScatter,
                            const PrPixelHit& h1,
                            const PrPixelHit& h2,
                            bool debug) const;

  /// Debugging methods
  bool matchKey(const PrPixelHit& hit) const {
    if (m_debugTool) {
      LHCb::LHCbID id = hit.id();
      return m_debugTool->matchKey(id, m_wantedKey);
    }
    return false;
  }

  void printHit(const PrPixelHit& hit, const std::string& title = "") const;
  void printTrack(PrPixelTrack& track) const;
  void printHitOnTrack(const PrPixelHit& hit, const bool ifMatch = true) const;

  /// count hits for monitoring
  std::tuple<unsigned int, unsigned int> countHits(const std::vector<PrPixelModuleHits>& modulehits) const;

  /// monitoring function
  StatusCode monitor(const std::vector<PrPixelModuleHits>& modulehits,
                     const LHCb::Tracks& tracks,
                     const LHCb::VPClusters& clusters) const;

  /// Hit manager tool
  ToolHandle<PrPixelHitManager> m_hitManager { "PrPixelHitManager", this };

  /// Properties
  Gaudi::Property<float> m_maxXSlope{this, "MaxXSlope", 0.400, "X Slope limit for seed pairs"};
  Gaudi::Property<float> m_maxYSlope{this, "MaxYSlope", 0.400, "Y Slope limit for seed pairs"};
  Gaudi::Property<float> m_extraTol{this, "ExtraTol", 0.6 * Gaudi::Units::mm, "Tolerance window when adding hits"};
  Gaudi::Property<unsigned int> m_maxMissed{this, "MaxMissed", 3, "Number of modules without a hit after which to stop extrapolation"};
  Gaudi::Property<float> m_maxScatter{this, "MaxScatter", 0.004, "Acceptance criteria for adding new hits"};
  Gaudi::Property<float> m_maxChi2Short{this, "MaxChi2Short", 20.0, "Acceptance criteria for track candidates : Max. chi2 for 3-hit tracks"};
  Gaudi::Property<float> m_fractionUnused{this, "FractionUnused", 0.5, "Acceptance criteria for track candidates : Min. fraction of unused hits"};
  Gaudi::Property<bool> m_stateClosestToBeamKalmanFit{this, "ClosestToBeamStateKalmanFit", true, "Parameter for Kalman fit"};
  Gaudi::Property<bool> m_stateEndVeloKalmanFit{this, "EndVeloStateKalmanFit", false, "Parameter for Kalman fit"};
  Gaudi::Property<bool> m_addStateFirstLastMeasurementKalmanFit{this, "AddFirstLastMeasurementStatesKalmanFit", false, "Parameter for Kalman fit"};
  Gaudi::Property<bool> m_runOnRawBanks{this, "RunOnRawBanks", true, "Use lite clusters or raw banks"};
  Gaudi::Property<unsigned int> m_maxClusterSize{this, "MaxClusterSize", VP::NPixelsPerSensor, "Maximum clusters size (no effect when running on lite cluster banks)"};
  Gaudi::Property<bool> m_trigger{this, "Trigger", false, "Are we running in the trigger?"};
  Gaudi::Property<bool> m_storecluster{this, "StoreClusters", true, "Do we want to store the clusters ?"};
  Gaudi::Property<int> m_wantedKey{this, "WantedKey", -100, "Parameter for debugging"};
  Gaudi::Property<bool> m_doTiming{this, "TimingMeasurement", false, "Parameter for debugging"};
  Gaudi::Property<std::string> m_debugToolName{this, "DebugToolName", "", "Parameter for debugging"};

  /// Debug control
  IPrDebugTool *m_debugTool = nullptr;

  /// Timing measurement control
  ISequencerTimerTool *m_timerTool;
  int m_timeTotal;
  int m_timePrepare;
  int m_timePairs;
  int m_timeFinal;
};
#endif  // PRPIXELTRACKING_H
