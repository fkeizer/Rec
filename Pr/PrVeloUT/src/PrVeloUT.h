#ifndef PRVELOUT_H
#define PRVELOUT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/Transformer.h"
// from TrackInterfaces
#include "TrackInterfaces/ITracksFromTrackR.h"
#include "Event/Track.h"
/** @class PrVeloUT PrVeloUT.h
   *
   *  PrVeloUT algorithm. This is just a wrapper,
   *  the actual pattern recognition is done in the 'PrVeloUTTool'.
   *
   *  - InputTracksName: Input location for Velo tracks
   *  - OutputTracksName: Output location for VeloTT tracks
   *  - TimingMeasurement: Do a timing measurement?
   *
   *  @author Mariusz Witek
   *  @date   2007-05-08
   *  @update for A-Team framework 2007-08-20 SHM
   *
   *  2017-03-01: Christoph Hasse (adapt to future framework)
   */

class PrVeloUT : public Gaudi::Functional::Transformer<LHCb::Tracks(const LHCb::Tracks&)>{
public:
  /// Standard constructor
  PrVeloUT( const std::string& name, ISvcLocator* pSvcLocator );

  virtual StatusCode initialize() override;    ///< Algorithm initialization

  LHCb::Tracks operator()(const LHCb::Tracks& inputTracks) const override;

private:

  Gaudi::Property<bool> m_doTiming{this, "TimingMeasurement", false};

  ITracksFromTrackR*   m_veloUTTool = nullptr;             ///< The tool that does the actual pattern recognition
  ISequencerTimerTool* m_timerTool  = nullptr;             ///< Timing tool
  int  m_veloUTTime                 = 0;                   ///< Counter for timing tool
  
};

#endif // PRVELOUT_H
