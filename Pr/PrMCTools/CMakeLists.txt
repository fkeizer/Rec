################################################################################
# Package: PrMCTools
################################################################################
gaudi_subdir(PrMCTools v2r12)

gaudi_depends_on_subdirs(Det/FTDet       
                         Det/VPDet
                         Det/STDet
                         Event/FTEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         GaudiAlg
                         Pr/PrFitParams
                         Pr/PrKernel
                         Pr/PrFitParams
                         Tr/TrackInterfaces)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(PrMCTools
                 src/*.cpp
                 INCLUDE_DIRS Event/FTEvent Pr/PrPixel
                 LINK_LIBRARIES LinkerEvent MCEvent GaudiAlgLib PrKernel VPDetLib STDetLib FTDetLib LoKiMCLib PrFitParamsLib)

