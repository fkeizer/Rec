#ifndef PRSTOREUTHIT_H
#define PRSTOREUTHIT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "PrKernel/UTHitInfo.h"
#include "PrKernel/UTHitHandler.h"
#include "Event/STLiteCluster.h"
#include "STDet/DeSTDetector.h"

/** @class PrStoreUTHit PrStoreUTHit.h
 *
 *
 *  @author Renato Quagliani, Christoph Hasse
 *  @date   2016-11-15
 */
//UTLiteCluster is fine too?
typedef FastClusterContainer<LHCb::STLiteCluster,int> UTLiteClusters;

class PrStoreUTHit : public Gaudi::Functional::Transformer<UT::HitHandler(const UTLiteClusters&)> {
public:
  /// Standard constructor
  PrStoreUTHit( const std::string& name, ISvcLocator* pSvcLocator );

  ///initialization
  virtual StatusCode initialize() override;
  ///main method
  UT::HitHandler operator()(const UTLiteClusters&) const override;

  ///
  void StoreUTHits(UT::HitHandler& hitHandler, const UTLiteClusters&clus )const;

  StatusCode BuildGeometry();
protected:

private:
  DeSTDetector *m_utDet;
  // DeUTDetector *m_utDet;

};
#endif // PRSTOREUTHIT_H
