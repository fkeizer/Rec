// include files
#include <type_traits>
#include "Relations/Relation1D.h"
#include "InCaloAcceptanceAlg.h"

// ============================================================================
/** @file
 *  Implementation file for class InCaloAcceptanceAlg
 *  @Date 2006-06-17
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

DECLARE_COMPONENT( InCaloAcceptanceAlg )

// ============================================================================
/// Standard protected constructor
// ============================================================================

InCaloAcceptanceAlg::InCaloAcceptanceAlg(const std::string& name,
                                         ISvcLocator* pSvc)
    : MergingTransformer(name, pSvc, {"Inputs", {/*"CRASHME"*/}},
                         {"Output", {}}) {
  // context-dependent default track container
  // (Context only available after baseclass is contructed)
  Gaudi::Functional::updateHandleLocations(
      *this, "Inputs", LHCb::CaloAlgUtils::TrackLocations(context()));
  declareProperty("Tool", m_tool);
}

// ============================================================================
// algorithm initialization
// ============================================================================

StatusCode InCaloAcceptanceAlg::initialize()
{
  StatusCode sc = MergingTransformer::initialize();
  if ( sc.isFailure() ) { return sc ; }
  sc = m_tool.retrieve();
  if ( sc.isFailure() ) { return sc ; }
  return StatusCode::SUCCESS;
}

// ============================================================================
// algorithm execution
// ============================================================================

Table InCaloAcceptanceAlg::operator()(const TrackLists& inputs) const {
  // a trivial check
  Assert(m_tool, "InAcceptance-tool  is invalid!");

  Table table(100);

  size_t nTracks = 0;
  size_t nAccept = 0;
  // loop over all track containers
  for (const auto& input : inputs) {
    if (!input) continue;
    // loop over all tracks in the container
    for (const LHCb::Track* track : *input) {
      ++nTracks;
      if (!use(track)) {
        continue;
      }  // CONTINUE
      const bool result = m_tool->inAcceptance(track);
      // fill the relation table
      table.i_push(track, result);  // ATTENTION: i-push is used
      if (result) {
        ++nAccept;
      }
    }
  }

  if (0 == nTracks) {
    if (msgLevel(MSG::DEBUG))
      debug() << "No good tracks have been selected" << endmsg;
  }
  // MANDATORY: i_sort after i_push
  table.i_sort();

  // a bit of statistics
  if (statPrint() || msgLevel(MSG::DEBUG)) {
    if (counterStat->isVerbose()) counter("#tracks") += nTracks;
    if (counterStat->isVerbose()) counter("#accept") += nAccept;
    // if(counterStat->isVerbose())counter ( "#links"  ) +=
    // table->i_relations().size() ;
    if (counterStat->isQuiet()) {
      std::string inputs;
      getProperty("Inputs", inputs).ignore();
      counter(inputs + "=>" + outputLocation()) += table.i_relations().size();
    }
  }
  return table;
}
