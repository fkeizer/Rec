// ============================================================================
// Include files 
// ============================================================================
#include "GaudiAlg/FunctionalUtilities.h"
// ============================================================================
// DetDesc
// ============================================================================
#include "DetDesc/IGeometryInfo.h"
// ============================================================================
// Event 
// ============================================================================
#include "Event/CaloDigit.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CellID.h"
// ============================================================================
// CaloUtils 
// ============================================================================
#include "CaloUtils/ClusterFunctors.h"
#include "CaloUtils/CaloAlgUtils.h"
// ============================================================================
// local
// ============================================================================
#include "CellularAutomatonAlg.h"
// ============================================================================
/** @file 
 *  Implementation file for class : CellularAutomatonAlg
 * 
 *  @date 2008-04-03 
 *  @author Victor Egorychev
 */
// ============================================================================
// Declaration of the Algorithm Factory
// ============================================================================
DECLARE_COMPONENT( CellularAutomatonAlg )
// ============================================================================
// Standard constructor, initializes variables
// ============================================================================
CellularAutomatonAlg::CellularAutomatonAlg( const std::string& name, 
                                            ISvcLocator*       pSvcLocator )
: Transformer ( name, pSvcLocator,
                KeyValue{"InputData" , LHCb::CaloDigitLocation::Ecal },
                KeyValue{"OutputData", LHCb::CaloClusterLocation::Ecal } )
{
  // set default data as a function of detector
  m_detData = LHCb::CaloAlgUtils::DeCaloLocation( name ) ;

  updateHandleLocation(*this,"InputData" , LHCb::CaloAlgUtils::CaloDigitLocation  ( name , context() ));
  updateHandleLocation(*this,"OutputData", LHCb::CaloAlgUtils::CaloClusterLocation( name , context() ));
}

// ============================================================================
// Initialization
// ============================================================================
StatusCode CellularAutomatonAlg::initialize() 
{
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  
  /// Retrieve geometry of detector
  m_detector = getDet<DeCalorimeter>( m_detData );
  if( !m_detector ) { return StatusCode::FAILURE; }
  
  // Tool Interface
  m_tool      = tool<ICaloClusterization>(m_toolName, this);
  counterStat = tool<ICounterLevel>("CounterLevel");
  //m_counter.setLevel(
  return StatusCode::SUCCESS;
}
// ============================================================================
// Main execution
// ============================================================================
LHCb::CaloCluster::Container 
CellularAutomatonAlg::operator()(const LHCb::CaloDigits& digits) const   ///< Algorithm execution
{
  // Create the container of clusters
  LHCb::CaloCluster::Container output;
  // update the version number (needed for serialization)
  output.setVersion( 2 ) ;
  
  // create vector of pointers for CaloCluster    
  std::vector<LHCb::CaloCluster*> clusters;
  
  // clusterization tool which return the vector of pointers for CaloClusters
  StatusCode sc;
  if( m_neig_level> 0){
    std::vector<LHCb::CaloCellID> seeds;
    sc= m_tool->clusterize(clusters, &digits, m_detector, seeds, m_neig_level) ;
  } else{
    sc = m_tool->clusterize(clusters, &digits, m_detector) ;
  }
  
  if ( sc.isFailure() )
  { throw GaudiException( " Failure from the tool, no clusterization!", name() , sc ); }
  
  // put to the container of clusters
  for ( const auto& clus : clusters ) { output.insert( clus ) ; }
  
  /** sort the sequence to simplify the comparison 
   *  with other clusterisation techniques 
   */
  if ( m_sort )  { 
    if ( !m_sortByET ) {
      // sorting criteria: Energy
      // perform the sorting 
      std::stable_sort    ( clusters.begin()            ,
                            clusters.end  ()            ,
                            LHCb::CaloDataFunctor::inverse( LHCb::CaloDataFunctor::Less_by_Energy ) ) ;
    } else {
      // sorting criteria : Transverse Energy
      LHCb::CaloDataFunctor::Less_by_TransverseEnergy<const DeCalorimeter*> Cmp ( m_detector ) ;
      // perform the sorting 
      std::stable_sort   ( clusters.begin()            ,
                           clusters.end  ()            ,
                           LHCb::CaloDataFunctor::inverse( Cmp ) ) ;    
    }
  }
  
  // statistics
  m_pass += (double) m_tool->iterations();
  m_clus += (double) output.size();
  m_event += 1;
  if (m_tool->iterations() < m_passMin) m_passMin = m_tool->iterations();
  if (m_tool->iterations() > m_passMax) m_passMax = m_tool->iterations();
  
  if( counterStat->isQuiet()   ) {
    std::string outputName; getProperty("OutputData",outputName).ignore();
    counter ( "#clusters => '" + outputName + "'" ) += output.size() ;
  }
  if( counterStat->isVerbose() )counter ( "Clusterisation pass") += m_tool->iterations();
 
  if (UNLIKELY( msgLevel( MSG::DEBUG) )){
    debug() << "Built " << clusters.size() <<" cellular automaton clusters  with " 
            << m_tool->iterations() << " iterations" <<endmsg;
    debug() << " ----------------------- Cluster List : " << endmsg;
    for(const auto& c : clusters ) {
      debug() << " Cluster seed " << c->seed() 
              << " energy " << c->e() 
              << " #entries " << c->entries().size() 
              << endmsg;
    }
  }
  return output;
}
// ============================================================================
//  Finalize
// ============================================================================
StatusCode CellularAutomatonAlg::finalize() 
{
  double avePass = 0.;
  double aveClus = 0.;
  if(m_event>0) {
    avePass = m_pass/m_event;
    aveClus = m_clus/m_event;
  }
  info() << "Built <" << aveClus 
         <<"> cellular automaton clusters/event  with <" 
         << avePass  << "> iterations (min,max)=(" << m_passMin << "," << m_passMax << ") on average " << endmsg;
  
  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
// =============================================================================
// the END 
// =============================================================================
