################################################################################
# Package: RichFutureRecMonitors
################################################################################

gaudi_subdir(RichFutureRecMonitors v1r0)

gaudi_depends_on_subdirs(Rich/RichFutureRecBase
                         Rich/RichUtils
                         Tr/TrackInterfaces)

find_package(ROOT)
find_package(Boost)
find_package(Vc)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                           ${Vc_INCLUDE_DIR})

gaudi_add_module(RichFutureRecMonitors
                 src/*.cpp
                 INCLUDE_DIRS Boost AIDA Rich/RichUtils Tr/TrackInterfaces
                 LINK_LIBRARIES RichUtils RichFutureRecBase)

gaudi_install_python_modules()

# Fixes for GCC7.
if( BINARY_TAG_COMP_NAME STREQUAL "gcc" AND BINARY_TAG_COMP_VERSION VERSION_GREATER "6.99")
  set_property(TARGET RichFutureRecMonitors APPEND PROPERTY COMPILE_FLAGS " -faligned-new ")
endif()
