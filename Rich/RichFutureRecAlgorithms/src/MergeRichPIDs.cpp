
// local
#include "MergeRichPIDs.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

MergeRichPIDs::MergeRichPIDs( const std::string& name, ISvcLocator* pSvcLocator )
  : MergingTransformer( name, pSvcLocator,
                        { "InputRichPIDLocations", { } },
                        { "OutputRichPIDLocation", LHCb::RichPIDLocation::Default } )
{ }
                            
//=============================================================================

LHCb::RichPIDs 
MergeRichPIDs::operator()( const vector_of_const_<LHCb::RichPIDs>& inPIDs ) const
{
  LHCb::RichPIDs outPIDs;
  
  // Loop over inputs and clone into output
  for ( const auto& pids : inPIDs )
  {
    outPIDs.reserve( outPIDs.size() + pids.size() );
    for ( const auto* pid : pids ) 
    {
      // Make a new cloned RichPID object.
      auto newPID = std::make_unique<LHCb::RichPID>( *pid );

      // Should we try and preserve the original PID key ?
      if ( m_keepPIDKey ) { outPIDs.insert( newPID.get(), pid->key() ); }
      else                { outPIDs.insert( newPID.get()             ); }

      // if get here, insertion was OK so give up ownership
      newPID.release();
    }
  }

  return outPIDs;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MergeRichPIDs )

//=============================================================================
