
// local
#include "RichGeomEffCKMassRings.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

GeomEffCKMassRings::GeomEffCKMassRings( const std::string& name,
                                        ISvcLocator* pSvcLocator )
  : MultiTransformer ( name, pSvcLocator,
                       { KeyValue{ "TrackSegmentsLocation",   LHCb::RichTrackSegmentLocation::Default },
                         KeyValue{ "CherenkovAnglesLocation",      CherenkovAnglesLocation::Emitted },
                         KeyValue{ "MassHypothesisRingsLocation",  MassHypoRingsLocation::Emitted } },
                       { KeyValue{ "GeomEffsLocation",             GeomEffsLocation::Default },
                         KeyValue{ "GeomEffsPerPDLocation",        GeomEffsPerPDLocation::Default },
                         KeyValue{ "SegmentPhotonFlagsLocation",   SegmentPhotonFlagsLocation::Default } } )
{ }

//=============================================================================

OutData
GeomEffCKMassRings::operator()( const LHCb::RichTrackSegment::Vector& segments,
                                const CherenkovAngles::Vector& ckAngles,
                                const MassHypoRingsVector& massRings ) const
{
  // Scalar type
  using ScType = GeomEffs::Type;

  // make the data to return
  OutData data;
  auto & geomEffsV      = std::get<GeomEffs::Vector>(data);
  auto & geomEffsPerPDV = std::get<GeomEffsPerPDVector>(data);
  auto & segPhotFlags   = std::get<SegmentPhotonFlags::Vector>(data);

  // reserve sizes
  geomEffsV.reserve     ( ckAngles.size() );
  geomEffsPerPDV.reserve( ckAngles.size() );
  segPhotFlags.reserve  ( ckAngles.size() );

  // iterate over input data
  for ( const auto && data : Ranges::ConstZip(segments,ckAngles,massRings) )
  {
    const auto & segment   = std::get<0>(data);
    const auto & ckAngles  = std::get<1>(data);
    const auto & massRings = std::get<2>(data);

    // make new objects for the geom effs
    geomEffsV.emplace_back( );
    auto & geomEffs = geomEffsV.back();
    geomEffsPerPDV.emplace_back( );
    auto & geomEffsPerPD = geomEffsPerPDV.back();

    // make entry for segment photon flags
    segPhotFlags.emplace_back( );
    auto & photFlags = segPhotFlags.back();

    // Which rich
    const auto rich = segment.rich();

    // Loop over PID types
    for ( const auto id : activeParticles() )
    {
      //The efficiency
      ScType eff = 0;
      
      // above threshold ?
      if ( ckAngles[id] > 0 )
      {
        // count the number of detected points
        std::size_t nDetect(0);

        // number of points
        const auto nPoints = massRings[id].size();

        // PD increment
        const auto pdInc = 1.0 / static_cast<float>(nPoints);

        // Loop over the points of the mass ring
        for ( const auto& P : massRings[id] )
        {
          if ( RayTracedCKRingPoint::InHPDTube == P.acceptance() )
          {
            // The HPD ID
            const auto hpdID = P.smartID().pdID();

            // count detected photons
            ++nDetect;

            // Increment PD map
            (geomEffsPerPD[id])[hpdID] += pdInc;
            
            // Segment photon flags
            photFlags.setInAcc( rich, P.globalPosition() );

          }
        }

        // compute the final eff
        eff = static_cast<ScType>(nDetect)/static_cast<ScType>(nPoints);

      }

      // save the final eff
      geomEffs.setData(id,eff);

    }

  }

  // return the new data
  return data;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GeomEffCKMassRings )

//=============================================================================
