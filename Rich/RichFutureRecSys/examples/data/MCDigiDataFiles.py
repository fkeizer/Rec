from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/MC/Run2/XDIGI/",   # Cambridge
    "/home/chris/LHCb/Data/MC/Run2/XDIGI/"   # CRJ's CernVM
    ]

data = [ ]
for path in searchPaths :
    files = sorted(glob.glob(path+"*.xdigi"))
    data += [ "'PFN:"+file for file in files ]

IOHelper('ROOT').inputFiles( data, clear=True )
FileCatalog().Catalogs = [ 'xmlcatalog_file:out.xml' ]

from Configurables import Brunel, LHCbApp

Brunel().InputType = 'DIGI'
Brunel().DataType  = "2015"
Brunel().Simulation = True
Brunel().WithMC     = True 
LHCbApp().DDDBtag   = "dddb-20140729"
LHCbApp().CondDBtag = "sim-20140730-vc-md100"
