from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/MC/Upgrade/XDIGI/",   # Cambridge
    "/home/chris/LHCb/Data/MC/Upgrade/XDIGI/"   # CRJ's CernVM
    ]

print "Data Files :-"
data = [ ]
for path in searchPaths :
    files = sorted(glob.glob(path+"*.xdigi"))
    data += [ "'PFN:"+file for file in files ]
    print("\n".join(files))

IOHelper('ROOT').inputFiles( data, clear=True )
FileCatalog().Catalogs = [ 'xmlcatalog_file:out.xml' ]

from Configurables import Brunel, LHCbApp

Brunel().InputType = 'DIGI'
Brunel().DataType = "Upgrade"
Brunel().Simulation = True
Brunel().WithMC     = True 
LHCbApp().DDDBtag   = "dddb-20170301"
LHCbApp().CondDBtag = "sim-20170301-vc-md100"
