from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/MC/Upgrade/XDST/",         # Cambridge
    "/home/chris/LHCb/Data/MC/Upgrade/XDST/",          # CRJ's CernVM
    "/group/rich/jonesc/LHCb/Data/MC/Upgrade/XDST/"    # the pit
    ]

print "Data Files :-"
data = [ ]
for path in searchPaths :
    files = sorted(glob.glob(path+"*.xdst"))
    data += [ "PFN:"+file for file in files ]
    print("\n".join(files))

IOHelper('ROOT').inputFiles( data, clear=True)
FileCatalog().Catalogs = [ 'xmlcatalog_file:out.xml' ]

from Configurables import Brunel, LHCbApp

LHCbApp().Simulation = True
LHCbApp().DataType  = "Upgrade"
LHCbApp().DDDBtag   = "dddb-20170301"
LHCbApp().CondDBtag = "sim-20170301-vc-md100"

from Configurables import CondDB
CondDB().setProp("Upgrade", True)

#Brunel().InputType    = "DST"
#Brunel().SkipTracking = True
#Brunel().DataType   = LHCbApp().DataType
#Brunel().Simulation = LHCbApp().Simulation
#Brunel().RecoSequence  = Brunel().RichSequences + [ "PROTO" ]

#from Configurables import DstConf
#DstConf().setProp("EnableUnpack", ["Tracking"] )
#ApplicationMgr().ExtSvc += [ "DataOnDemandSvc" ]
