#ifndef DICT_RICHRECUTILSDICT_H 
#define DICT_RICHRECUTILSDICT_H 1

#include "RichUtils/RichObjPtn.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"
#include "RichRecUtils/RichRecPhotonKey.h"
#include "RichRecUtils/RichTrackID.h"
#include "RichRecUtils/RichCKResolutionFitter.h"

// instantiate some templated classes, to get them into the dictionary
namespace 
{
  struct _Instantiations 
  {
    Rich::ObjPtn<Rich::Rec::RadCorrLocalPositions> obj_1;
    Rich::Rec::CKResolutionFitter                  obj_2;
  };
}

#endif // DICT_RICHRECUTILSDICT_H
