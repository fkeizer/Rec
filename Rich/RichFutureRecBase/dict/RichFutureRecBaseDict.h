#ifndef DICT_RICHRECBASEDICT_H 
#define DICT_RICHRECBASEDICT_H 1

#include "RichRecUtils/RichRecPhotonKey.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// instantiate some templated classes, to get them into the dictionary
namespace 
{
  struct _Instantiations 
  {
    // nothing to do
  };
}

#endif // DICT_RICHRECBASEDICT_H
