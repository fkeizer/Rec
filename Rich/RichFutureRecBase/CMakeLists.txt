################################################################################
# Package: RichFutureRecBase
################################################################################
gaudi_subdir(RichFutureRecBase v1r0)

gaudi_depends_on_subdirs(Det/RichDet
                         Event/RecEvent
                         Rich/RichFutureKernel
                         Rich/RichFutureRecEvent
                         Rich/RichRecUtils)
find_package(Boost)
find_package(ROOT)
find_package(Vc)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                           ${Vc_INCLUDE_DIR})

gaudi_add_library(RichFutureRecBase
                  src/*.cpp
                  PUBLIC_HEADERS RichFutureRecBase
                  LINK_LIBRARIES RichDetLib RecEvent RichFutureKernel RichFutureRecEvent RichRecUtils)

#gaudi_add_dictionary(RichFutureRecBase
#                     dict/RichFutureRecBaseDict.h
#                     dict/RichFutureRecBaseDict.xml
#                     LINK_LIBRARIES RichDetLib RecEvent RichFutureKernel RichRecUtils RichRecEvent RichRecBase
#                     OPTIONS "-U__MINGW32__ -DBOOST_DISABLE_ASSERTS")
