from os import environ
from Gaudi.Configuration import *
import GaudiKernel.ProcessJobOptions
from TrackSys.Configuration import *
from GaudiKernel.SystemOfUnits import mm
from GaudiKernel.SystemOfUnits import GeV

# Sanity checks
def CheckTrackTypes(trackTypes, subDets, seqType):

    trackTypes = TrackSys().getProp("TrackTypes")

    if "Velo" in trackTypes:
        if not (("VP" in subDets)):
            raise RuntimeError("Specify VP.")

    if "Forward" in trackTypes:
        if not ("Velo" in trackTypes):
            log.warning("Velo tracks added to tracking sequence.")
            trackTypes += ["Velo"]
        if not (("FT" in subDets)):
            raise RuntimeError("Specify T-Stations.")

    if seqType == "Best":
        if "Upstream" in trackTypes:
            if not ("Velo" in trackTypes):
                log.warning("Velo tracks added to tracking sequence.")
                trackTypes += ["Velo"]
                if not (("UT" in subDets)):
                    raise RuntimeError("Specify UT.")

        if "Downstream" in trackTypes:
            if not ("Seeding" in trackTypes):
                log.warning("Seed tracks added to tracking sequence.")
                trackTypes += ["Seeding"]
                if not (("UT" in subDets)):
                    raise RuntimeError("Specify UT.")


        if "Seeding" in trackTypes:
            if not ("FT" in subDets) :
                raise RuntimeError("Specify T-Stations.")

        if "Match" in trackTypes:
            if not ("Velo" in trackTypes):
                log.warning("Velo tracks added to tracking sequence.")
                trackTypes += ["Velo"]

            if not ("Seeding" in trackTypes):
                log.warning("Seed tracks added to tracking sequence.")
                trackTypes += ["Seeding"]

    return trackTypes

# Get sub detectors
def GetSubDets():
    subDets = []
    from Configurables import LHCbApp
    #Test if LHCbApp has this method (same revision as property)
    if hasattr(LHCbApp(),"Detectors"):
        if LHCbApp().isPropertySet("Detectors"):
            subDets = LHCbApp().upgradeDetectors()

    return subDets

#Decoding
def DecodeTracking(subDets):

    decodingSeq = GaudiSequencer("RecoDecodingSeq")
    from DAQSys.Decoders import DecoderDB
    from DAQSys.DecoderClass import decodersForBank
    decs=[]
    # Are these the right decoders?
    if "VP" in subDets:
        decs=decs+decodersForBank(DecoderDB,"VP")
    if "UT" in subDets:
        decs=decs+decodersForBank(DecoderDB,"UT")
        from Configurables import STOfflinePosition
        UT = STOfflinePosition('ToolSvc.UTClusterPosition')
        UT.DetType = "UT"
    if "FT" in subDets:
        decs=decs+decodersForBank(DecoderDB,"FTCluster")

    decodingSeq.Members+=[d.setup() for d in decs]
    
    # In case we have UT and FT clusters we can run the GECFilter
    if "UT" in subDets and "FT" in subDets:
        from Configurables import PrGECFilter
        decodingSeq.Members+= [PrGECFilter()]
   
    if "UT" in subDets:
        from Configurables import PrStoreUTHit
        decodingSeq.Members+=[PrStoreUTHit()]
    if "FT" in subDets:
        from Configurables import PrStoreFTHit
        decodingSeq.Members+=[PrStoreFTHit()]


# Set sequence for Velo Pix
def RecoVeloPr(seqType = "Fast", fit = True, output_tracks="Rec/Track/Velo", output_tracks_fitted= "Rec/Track/FittedHLT1VeloTracks"):
    from Configurables import PrPixelTracking
    prPixelTracking = PrPixelTracking("PrPixelTracking"+seqType)
    prPixelTracking.OutputTracksName = output_tracks
    prPixelTracking.ClosestToBeamStateKalmanFit = True
    #prPixelTracking.EndVeloStateKalmanFit = True
    #prPixelTracking.AddFirstLastMeasurementStatesKalmanFit = True
    GaudiSequencer("RecoTr"+seqType+"Seq").Members += [ prPixelTracking ]

    #Kalman filter for VELO tracks, needed for PV reconstruction
#    if fit:
#        from Configurables import TrackEventFitter, TrackMasterFitter
#        from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
#        veloFitter = TrackEventFitter('VeloOnlyFitterAlg'+seqType)
#        veloFitter.TracksInContainer = output_tracks
#        veloFitter.TracksOutContainer = output_tracks_fitted
#        veloFitter.addTool(TrackMasterFitter, name="Fitter")
#        ConfiguredMasterFitter( veloFitter.Fitter)
#        GaudiSequencer("RecoTr"+seqType+"Seq").Members += [ veloFitter ]

# Set Primary Vertex reconstruction
def RecoPV(seqType = "Fast", input_tracks = "Rec/Track/FittedHLT1VeloTracks"):
    from Configurables import PatPV3D, PVOfflineTool, LSAdaptPV3DFitter

    pvAlg = PatPV3D("PatPV3D")
    pvAlg.InputTracks = input_tracks
    pvAlg.addTool(PVOfflineTool,"PVOfflineTool")
    pvAlg.PVOfflineTool.addTool(LSAdaptPV3DFitter, "LSAdaptPV3DFitter")
    pvAlg.PVOfflineTool.PVFitterName = "LSAdaptPV3DFitter"
    pvAlg.PVOfflineTool.LSAdaptPV3DFitter.UseFittedTracks = True
    pvAlg.PVOfflineTool.LSAdaptPV3DFitter.AddMultipleScattering = False
    pvAlg.PVOfflineTool.LSAdaptPV3DFitter.TrackErrorScaleFactor = 1.0
    pvAlg.PVOfflineTool.LSAdaptPV3DFitter.MinTracks = 4
    pvAlg.PVOfflineTool.LSAdaptPV3DFitter.trackMaxChi2 = 12.0
    pvAlg.PVOfflineTool.UseBeamSpotRCut = True
    pvAlg.PVOfflineTool.BeamSpotRCut = 0.2
    pvAlg.PVOfflineTool.BeamSpotRHighMultiplicityCut = 0.4
    pvAlg.PVOfflineTool.BeamSpotRMultiplicityTreshold = 10
    pvAlg.OutputVerticesName = "Rec/Vertex/Primary"
    pvAlg.PrimaryVertexLocation = "Rec/Vertex/PrimaryVertices"


    GaudiSequencer("RecoTr"+seqType+"Seq").Members += [ pvAlg ];
    GaudiSequencer("RecoTr"+seqType+"Seq").IgnoreFilterPassed = True

# Set Upstream tracking
def RecoUpstream(seqType = "Fast", min_pt=0, input_tracks = "Rec/Track/Velo", output_tracks = "Rec/Track/Upstream"):
    from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter

    from Configurables import PrVeloUT, PrVeloUTTool
    prVeloUT = PrVeloUT("PrVeloUT"+seqType)
    prVeloUT.InputTracksName = input_tracks
    prVeloUT.OutputTracksName = output_tracks
    prVeloUT.addTool(PrVeloUTTool, "PrVeloUTTool")
    prVeloUT.PrVeloUTTool.minPT = min_pt*GeV
    from Configurables import TrackMasterFitter
    prVeloUT.addTool(TrackMasterFitter,"Fitter")
    ConfiguredMasterFitter( prVeloUT.Fitter)
    GaudiSequencer("RecoTr"+seqType+"Seq").Members += [ prVeloUT ]

#Set Foward tracking
def RecoForward(seqType = "Fast",
                min_pt=0.05*GeV,
                input_tracks = "Rec/Track/Velo",
                output_tracks = "Rec/Track/Forward",
                fit = True,
                simplifiedGeometry = True,
                output_tracks_fitted = "Rec/Track/FittedForward",
                tuning = 0):

    from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
    from Configurables import TrackUsedLHCbID

    from Configurables import PrForwardTracking, PrForwardTool
    prFwdTracking = PrForwardTracking("PrForwardTracking"+seqType)
    prFwdTracking.InputName = input_tracks
    prFwdTracking.OutputName = output_tracks
    prFwdTracking.addTool(PrForwardTool, "PrForwardTool")
    prFwdTracking.PrForwardTool.MinPT = min_pt
    if seqType == "Fast":
        if tuning == 0:
          prFwdTracking.PrForwardTool.UseMomentumEstimate = True
          prFwdTracking.PrForwardTool.Preselection = True
          prFwdTracking.PrForwardTool.PreselectionPT = 300.
          prFwdTracking.PrForwardTool.TolYTriangleSearch = 20.
          prFwdTracking.PrForwardTool.TolYCollectX = 3.5
          prFwdTracking.PrForwardTool.TolYSlopeCollectX = 0.001
          prFwdTracking.PrForwardTool.MinXHits = 5
          prFwdTracking.PrForwardTool.MaxXWindow = 1.
          prFwdTracking.PrForwardTool.MaxXWindowSlope = 0.002
          prFwdTracking.PrForwardTool.MaxXGap = 1.
          prFwdTracking.PrForwardTool.SecondLoop = True
          prFwdTracking.PrForwardTool.MinXHits2nd = 4
          prFwdTracking.PrForwardTool.MaxXWindow2nd = 1.5
          prFwdTracking.PrForwardTool.MaxXWindowSlope2nd = 0.002
          prFwdTracking.PrForwardTool.MaxXGap2nd = 0.5
        if tuning == 1:
          prFwdTracking.PrForwardTool.UseMomentumEstimate = True
          prFwdTracking.PrForwardTool.Preselection = True
          prFwdTracking.PrForwardTool.PreselectionPT = 300.
          prFwdTracking.PrForwardTool.TolYTriangleSearch = 20.
          prFwdTracking.PrForwardTool.TolYCollectX = 2.7
          prFwdTracking.PrForwardTool.TolYSlopeCollectX = 0.0007
          prFwdTracking.PrForwardTool.MinXHits = 5
          prFwdTracking.PrForwardTool.MaxXWindow = 0.7
          prFwdTracking.PrForwardTool.MaxXWindowSlope = 0.0015
          prFwdTracking.PrForwardTool.MaxXGap = 0.3
          prFwdTracking.PrForwardTool.SecondLoop = True
          prFwdTracking.PrForwardTool.MinXHits2nd = 4
          prFwdTracking.PrForwardTool.MaxXWindow2nd = 0.7
          prFwdTracking.PrForwardTool.MaxXWindowSlope2nd = 0.0015
          prFwdTracking.PrForwardTool.MaxXGap2nd = 0.3

    else:
        prFwdTracking.PrForwardTool.UseMomentumEstimate = False
        prFwdTracking.PrForwardTool.Preselection = False
        prFwdTracking.PrForwardTool.TolYTriangleSearch = 20.
        prFwdTracking.PrForwardTool.TolYCollectX = 4.1
        prFwdTracking.PrForwardTool.TolYSlopeCollectX = 0.0018
        prFwdTracking.PrForwardTool.MinXHits = 5
        prFwdTracking.PrForwardTool.MaxXWindow = 1.2
        prFwdTracking.PrForwardTool.MaxXWindowSlope = 0.002
        prFwdTracking.PrForwardTool.MaxXGap = 1.2
        prFwdTracking.PrForwardTool.SecondLoop = True
        prFwdTracking.PrForwardTool.MinXHits2nd = 4
        prFwdTracking.PrForwardTool.MaxXWindow2nd = 1.5
        prFwdTracking.PrForwardTool.MaxXWindowSlope2nd = 0.002
        prFwdTracking.PrForwardTool.MaxXGap2nd = 0.5

    GaudiSequencer("RecoTr"+seqType+"Seq").Members += [ prFwdTracking ]

    if fit:
        if "ParameterizedKalman" in TrackSys().getProp("ExpertTracking") and seqType == "Fast":
          from Configurables import ParameterizedKalmanFit
          ParameterizedKalman = ParameterizedKalmanFit('ForwardFitterAlgParam'+seqType)
          ParameterizedKalman.InputName = output_tracks
          ParameterizedKalman.OutputName = output_tracks_fitted
          ParameterizedKalman.MaxNumOutlier = 2
          
          from Configurables import TrackMasterExtrapolator, SimplifiedMaterialLocator
          ParameterizedKalman.addTool( TrackMasterExtrapolator, name="extr")
          ParameterizedKalman.extr.ApplyMultScattCorr = True
          ParameterizedKalman.extr.ApplyEnergyLossCorr = False
          ParameterizedKalman.extr.ApplyElectronEnergyLossCorr = True
          ParameterizedKalman.extr.addTool(SimplifiedMaterialLocator, name = "MaterialLocator")
  
          GaudiSequencer("RecoTr"+seqType+"Seq").Members += [ ParameterizedKalman ]
        elif "vectorFitter" in TrackSys().getProp("ExpertTracking"):
          from Configurables import TrackEventFitter, TrackVectorFitter, TrackMasterFitter
          forwardFitter = TrackEventFitter('ForwardFitterAlgVector'+seqType)
          forwardFitter.TracksInContainer = output_tracks
          forwardFitter.TracksOutContainer = output_tracks_fitted
          vectorFitter = TrackVectorFitter()
          vectorFitter.MaxNumberOutliers = 2
          from Configurables import TrackParabolicExtrapolator, TrackMasterExtrapolator, SimplifiedMaterialLocator
          # Fetch options from MasterFitter MeasProvider
          ConfiguredMasterFitter(TrackMasterFitter(), SimplifiedGeometry = simplifiedGeometry)
          vectorFitter.MeasProvider = TrackMasterFitter().MeasProvider
          if "useParabolicExtrapolator" in TrackSys().getProp("ExpertTracking"):
            vectorFitter.addTool(TrackParabolicExtrapolator, name="Extrapolator")
          else:
            vectorFitter.addTool(TrackMasterExtrapolator, name="Extrapolator")
          vectorFitter.addTool(SimplifiedMaterialLocator, name = "MaterialLocator")
          vectorFitter.Extrapolator.addTool(SimplifiedMaterialLocator, name="MaterialLocator")

          forwardFitter.addTool(vectorFitter, name="Fitter")
          GaudiSequencer("RecoTr"+seqType+"Seq").Members += [forwardFitter]
        else:
          from Configurables import TrackEventFitter, TrackMasterFitter
          from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
          forwardFitter = TrackEventFitter('ForwardFitterAlg'+seqType)
          forwardFitter.TracksInContainer = output_tracks
          forwardFitter.TracksOutContainer = output_tracks_fitted
          forwardFitter.addTool(TrackMasterFitter, name="Fitter")
          ConfiguredMasterFitter( forwardFitter.Fitter, SimplifiedGeometry = simplifiedGeometry)
          GaudiSequencer("RecoTr"+seqType+"Seq").Members += [ forwardFitter ]

# Set Seeding
def RecoSeeding(seqType = "Best", output_tracks = "Rec/Track/Seed"):
    from Configurables import PrHybridSeeding
    prHybridSeeding = PrHybridSeeding("PrHybridSeeding"+seqType)
    prHybridSeeding.OutputName = output_tracks
    GaudiSequencer("RecoTr"+seqType+"Seq").Members += [prHybridSeeding]

# Set Matching
def RecoMatch(seqType = "Best", output_tracks = "Rec/Track/Match",
              input_seed = "Rec/Track/Seed", input_velo = "Rec/Track/Velo"):
    from Configurables import PrMatchNN
    prMatch = PrMatchNN("PrMatchNN"+seqType)
    prMatch.MatchOutput = output_tracks
    prMatch.VeloInput = input_velo
    prMatch.SeedInput = input_seed
    GaudiSequencer("RecoTr"+seqType+"Seq").Members += [ prMatch ]

# Set Downstream
def RecoDownstream( seqType = "Best", output_tracks = "Rec/Track/Downstream"):
    from Configurables import PrLongLivedTracking
    prDownstream = PrLongLivedTracking("PrLongLivedTracking"+seqType)
    prDownstream.OutputLocation = output_tracks
    GaudiSequencer("RecoTr"+seqType+"Seq").Members += [ prDownstream ]

# Fast tracking reconstruction, to be HLT1 like
def RecoFastTrackingStage(exclude=[], defTracks = {}, simplifiedGeometryFit = True):
    ## Start TransportSvc, needed by track fit  ???
    ApplicationMgr().ExtSvc.append("TransportSvc")
    subDets = GetSubDets()
    trackTypes = TrackSys().getProp("TrackTypes")

    ### Sanity checks
    sType = "Fast"
    trackTypes = CheckTrackTypes(trackTypes, subDets, sType)

    ### Do the decoding of the detectors
    DecodeTracking(subDets)

    ### Define the pattern recognition
    if "Velo" in trackTypes:
        RecoVeloPr(seqType = sType,
                   output_tracks = defTracks["Velo"]["Location"] ,
                   output_tracks_fitted = defTracks["VeloFitted"]["Location"])
        RecoPV(seqType=sType,
               input_tracks = defTracks["VeloFitted"]["Location"])
        defTracks["Velo"]["BestUsed"] = True

    if "Upstream" in trackTypes:
        RecoUpstream(seqType=sType,
                     min_pt = 0.3,
                     input_tracks = defTracks["Velo"]["Location"],
                     output_tracks = defTracks["Upstream"]["Location"])
        defTracks["Upstream"]["BestUsed"] = True

    if "Forward" in trackTypes:
        inType = 'Upstream' if 'Upstream' in trackTypes else 'Velo'
        RecoForward(seqType=sType,
                    min_pt = 0.4*GeV,
                    input_tracks = defTracks[inType]["Location"],
                    output_tracks = defTracks["ForwardFast"]["Location"],
                    fit = True,
                    simplifiedGeometry = simplifiedGeometryFit,
                    output_tracks_fitted = defTracks["ForwardFastFitted"]["Location"])
        defTracks["ForwardFastFitted"]["BestUsed"] = True

    return defTracks

# Best tracking reconstruction, to be like HLT2
def RecoBestTrackingStage(exclude=[], tracklists = [], defTracks = {}, simplifiedGeometryFit = True):
    ## Start TransportSvc, needed by track fit  ???
    ApplicationMgr().ExtSvc.append("TransportSvc")
    subDets = GetSubDets()
    trackTypes = TrackSys().getProp("TrackTypes")
    ### Sanity checks
    sType = "Best"
    trackTypes = CheckTrackTypes(trackTypes, subDets, sType)

    if "Forward" in trackTypes:
        # PLEASE NOTE: For now we need to take all VELO tracks, including that used in the fast stage
        RecoForward(seqType = sType,
                    min_pt = 0.05*GeV,
                    input_tracks = defTracks["Velo"]["Location"],
                    output_tracks = defTracks["ForwardBest"]["Location"],
                    fit = False)
        defTracks["ForwardBest"]["BestUsed"] = True

    if "Seeding" in trackTypes:
        RecoSeeding(seqType = sType,
                    output_tracks = defTracks["Seeding"]["Location"])
        defTracks["Seeding"]["BestUsed"] = True

    if "Match" in trackTypes:
        RecoMatch(seqType = sType,
                  output_tracks = defTracks["Match"]["Location"],
                  input_seed = defTracks["Seeding"]["Location"],
                  input_velo = defTracks["Velo"]["Location"])
        defTracks["Match"]["BestUsed"] = True

    if "Downstream" in trackTypes:
        RecoDownstream( seqType = sType,
                        output_tracks = defTracks["Downstream"]["Location"])
        defTracks["Downstream"]["BestUsed"] = True

    return defTracks

def RecoBestTrackCreator(exclude=[], seqType = "", defTracks = {}, simplifiedGeometryFit = True):

    tracklists = []
    for tr in defTracks:
        if ( defTracks[tr]["BestUsed"] == True ):
            tracklists += [defTracks[tr]["Location"]]

    # Do the Clone Killing and create Best tracks container
    bestSeq = GaudiSequencer("BestTrackCreatorSeq")
    GaudiSequencer("Reco"+seqType+"Seq").Members += [ bestSeq ]

    from Configurables import TrackBestTrackCreator, TrackMasterFitter
    from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
    bestTrackCreator = TrackBestTrackCreator( TracksInContainers = tracklists )
    bestTrackCreator.FitTracks = True
    bestTrackCreator.InitTrackStates = False
    bestTrackCreator.DoNotRefit = True

    if "vectorFitter" in TrackSys().getProp("ExpertTracking"):
      from Configurables import TrackVectorFitter
      vectorFitter = TrackVectorFitter()
      vectorFitter.MaxNumberOutliers = 2
      from Configurables import TrackParabolicExtrapolator, TrackMasterExtrapolator, SimplifiedMaterialLocator
      if "useParabolicExtrapolator" in TrackSys().getProp("ExpertTracking"):
        vectorFitter.addTool(TrackParabolicExtrapolator, name="Extrapolator")
      else:
        vectorFitter.addTool(TrackMasterExtrapolator, name="Extrapolator")
      vectorFitter.addTool(SimplifiedMaterialLocator, name = "MaterialLocator")
      vectorFitter.Extrapolator.addTool(SimplifiedMaterialLocator, name="MaterialLocator")
      bestTrackCreator.addTool(vectorFitter, name="Fitter")
    else:
      bestTrackCreator.addTool(TrackMasterFitter, name="Fitter")
      ConfiguredMasterFitter( bestTrackCreator.Fitter, SimplifiedGeometry = simplifiedGeometryFit )
    bestSeq.Members += [ bestTrackCreator ]

def ExtraInformations(seqType = ""):

    ## Extra track information sequence
    extraInfos = TrackSys().getProp("TrackExtraInfoAlgorithms")
    if len(extraInfos) > 0 :

        ## ghost probability using a Neural Net
        if "GhostProbability" in extraInfos :
            from Configurables import TrackAddNNGhostId
            ghostID = TrackAddNNGhostId()
            ghostID.GhostIdTool = "UpgradeGhostId"
            GaudiSequencer("TrackAddExtraInfoSeq").Members += [ ghostID ]


        addExtraInfoSeq = GaudiSequencer("TrackAddExtraInfoSeq")
        GaudiSequencer("Reco"+seqType+"Seq").Members += [ addExtraInfoSeq ]
