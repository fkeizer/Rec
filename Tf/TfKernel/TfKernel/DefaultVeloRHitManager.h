#ifndef INCLUDE_TF_DEFAULTVELORHITMANAGER_H
#define INCLUDE_TF_DEFAULTVELORHITMANAGER_H 1

#include "TfKernel/DefaultVeloHitManager.h"


namespace Tf {

  static const InterfaceID IID_DefaultVeloRHitManager( "Tf::DefaultVeloRHitManager", 1, 0 );

  /** @class DefaultVeloRHitManager DefaultVeloRHitManager.h
   *
   *  Default Hit manager for Velo R hits
   * 
   * @author Kurt Rinnert <kurt.rinnert@cern.ch>
   * @date   2007-07-31
   */
  struct DefaultVeloRHitManager : DefaultVeloHitManager<DeVeloRType,VeloRHit,4> {

    /// Retrieve interface ID
    static const InterfaceID& interfaceID() { return IID_DefaultVeloRHitManager; }

    /// Standard Constructor
    DefaultVeloRHitManager(const std::string& type,
                           const std::string& name,
                           const IInterface* parent);

  };

}
#endif // INCLUDE_TF_DEFAULTVELORHITMANAGER_H

