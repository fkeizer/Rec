#ifndef INCLUDE_PATVELOTTHYBRIDFIT_H
#define INCLUDE_PATVELOTTHYBRIDFIT_H 1

#include "GaudiAlg/GaudiTool.h"
#include "TrackInterfaces/IPatVeloTTFit.h"
#include "PatTTMagnetTool.h"
#include "PatKernel/PatTTHit.h"
#include "vdt/sqrt.h"

/** @class PatVeloTTHybridFit PatVeloTTHybridFit.h
 *  
 * provide an interface to the internal fit used in the PatVeloTTHybridTool
 * to initialize track states
 *
 * @author Michel De Cian, michel.de.cian@cern.ch
 * @date   2017-03-20
 */
class PatVeloTTHybridFit : public extends<GaudiTool, IPatVeloTTFit> {
public:

  /// Standard Constructor
  PatVeloTTHybridFit(const std::string& type, const std::string& name,
                     const IInterface* parent);

  StatusCode initialize() override; ///< Tool initialization

  StatusCode fitVTT( LHCb::Track& track) const override;

  void finalFit( const std::vector<PatTTHit*>& theHits, const std::array<float,8>& vars, std::array<float,3>& params ) const override;

private:
  
  DeSTDetector* m_ttDet = nullptr;
  PatTTMagnetTool*    m_PatTTMagnetTool = nullptr;  ///< Multipupose tool for Bdl and deflection

  
  
  


};
#endif // INCLUDE_PATVELOTTFIT_H

